#!/usr/bin/env sh

export APP_NAME=lapag-ledger
export DATABASE_IN_MEMORY=true
export DATABASE_NAME=:memory:
export LOG_LEVEL=debug
export SHOW_ERROR=true
export NODE_PATH=Src:Test


mocha --config .mocharc.yaml Test/**/Unit/**/*.spec.ts $1