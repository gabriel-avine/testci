set -e

if [ -e dist ]; then
  rm -rf dist
fi

export NODE_OPTIONS=--max_old_space_size=1024

npm install
npm run generate:docs
npm run build
npm prune --production
cp -r node_modules dist
cp package.json dist

cd dist

zip -r ../function.zip *
