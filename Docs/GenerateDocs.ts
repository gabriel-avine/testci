import fs from "fs";
import { default as yaml } from "js-yaml";
import { OpenAPI } from "openapi-types";
import { default as Swagger } from "swagger-parser";

import { version } from "../package.json";

const JSON_SPACE = 4;

const generate = (api: OpenAPI.Document): void => {
  const doc = yaml.dump({
    ...api,
    info: { ...api.info, version },
    servers: [{ url: "http://localhost:3000", description: "Local" }],
  });

  const json = JSON.stringify(yaml.load(doc), null, JSON_SPACE);

  fs.writeFileSync("Src/Shared/lapag-ledger-service.json", json, "utf-8");
  fs.writeFileSync("lapag-ledger-service.yml", doc, "utf-8");
};

// tslint:disable-next-line: no-floating-promises
Swagger.dereference("./Docs/base.swagger.yml").then(generate);
