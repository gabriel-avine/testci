import { assert, expect } from "chai";
import { ApplicationResult } from "Shared/Application/Entities/ApplicationResult";
import { ApplicationEvents } from "Shared/Application/Enums/ApplicationEvents";
import { StatusCode } from "Shared/Interfaces/Enums/StatusCode";
import { ExpressResult } from "Shared/Interfaces/Express/Entities/ExpressResult";

describe("Unit: Interface ExpressResult", (): void => {
  it("Should create error result", (): void => {
    // Given
    const ex = new Error("Invalid operation");

    // When
    const expressResult = ExpressResult.Error(ex);

    // Then
    assert.deepEqual(expressResult.status, StatusCode.ERROR);
    assert.deepEqual(expressResult.body, ex);
  });

  it("Should return NO_CONTENT if application is success and dont have message", (): void => {
    // Given
    const applicationResult = new ApplicationResult(ApplicationEvents.SUCCESS);

    // When
    const expressResult = new ExpressResult(applicationResult);

    // Then
    assert.equal(expressResult.status, StatusCode.NO_CONTENT);
    assert.isUndefined(expressResult.body);
  });

  it("Should return NOT_FOUND if application is not_found", (): void => {
    // Given
    const applicationResult = new ApplicationResult(ApplicationEvents.NOT_FOUND);

    // When
    const expressResult = new ExpressResult(applicationResult);

    // Then
    assert.deepEqual(expressResult.status, StatusCode.NOT_FOUND);
  });

  it("Should return exception if application result is not expected", (): void => {
    // Given
    const event: ApplicationEvents = ("teste" as unknown) as ApplicationEvents;
    const applicationResult = new ApplicationResult(event);

    // Then
    const createResult = (): ExpressResult => new ExpressResult(applicationResult);

    // Then
    expect(createResult).to.throw(`Invalid event ${event}`);
  });
});
