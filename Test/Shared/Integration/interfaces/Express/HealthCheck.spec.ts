import { StatusCode } from "Shared/Interfaces/Enums/StatusCode";
import { HttpServer } from "Shared/Interfaces/Express/HttpServer";
import { default as supertest, SuperTest, Test } from "supertest";

describe("Integration: Interfaces Express HealthCheck", (): void => {
  let api: SuperTest<Test>;

  before((): void => {
    api = supertest(HttpServer.Create());
  });

  it("Should return OK if is healthy", async (): Promise<void> => {
    await api.get("/health").expect(StatusCode.NO_CONTENT);
  });
});
