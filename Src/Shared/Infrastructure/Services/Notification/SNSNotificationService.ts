import { SNS } from "aws-sdk";
import { inject, injectable } from "inversify";
import { IConfig } from "Shared/Application/Contracts/IConfig";
import { ILogger } from "Shared/Application/Contracts/Infrastructure/ILogger";
import { INotificationService } from "Shared/Application/Contracts/Infrastructure/Services/INotificationService";
import { DomainEvent } from "Shared/Domain/DomainEvent";
import * as Types from "Shared/Interfaces/DI/Types";
import { SnsEvent } from "./Entities/SNSEvent";

@injectable()
class SNSNotificationService implements INotificationService {
  private readonly config: IConfig;
  private readonly logger: ILogger;
  private readonly sns: SNS;

  public constructor(@inject(Types.Config) config: IConfig, @inject(Types.Logger) logger: ILogger) {
    this.config = config;
    this.logger = logger;

    this.sns = new SNS({
      apiVersion: config.SnsApiVersion,
      region: config.SnsAwsRegion,
    });
  }

  public async send(event: DomainEvent): Promise<boolean> {
    try {
      this.logger.info("Domain event", { event });

      const snsEvent = new SnsEvent(this.config.SnsTopicArn, event);

      this.logger.debug("SnsEvent", { snsEvent });

      await this.sns.publish(snsEvent).promise();

      this.logger.info("Event posted");

      return true;
    } catch (ex) {
      this.logger.error("Erro to post event", ex, { event });

      return false;
    }
  }
}
export { SNSNotificationService };
