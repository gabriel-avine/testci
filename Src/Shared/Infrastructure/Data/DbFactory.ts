import { Config } from "Shared/Infrastructure/Config";
import { Connection, getConnectionManager } from "typeorm";
import { ConnectionOptions } from "typeorm/connection/ConnectionOptions";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { SqliteConnectionOptions } from "typeorm/driver/sqlite/SqliteConnectionOptions";
import { EntityMappings } from "./EntityMappings";
import { MigrationsList } from "./Migrations/MigrationsList";

class DbFactory {
  public static async Create(): Promise<Connection> {
    const config = new Config();

    const entities = EntityMappings.get();

    const migrations = MigrationsList.get();

    const sqlite: SqliteConnectionOptions = {
      database: config.DatabaseName,
      entities,
      migrations,
      migrationsRun: true,
      type: "sqlite",
    };

    const postgres: PostgresConnectionOptions = {
      database: config.DatabaseName,
      entities,
      host: config.DatabaseHost,
      logging: config.DatabaseLogging,
      migrations,
      password: config.DatabasePassword,
      port: config.DatabasePort,
      type: "postgres",
      username: config.DatabaseUser,
    };

    const connectionConfig: ConnectionOptions = config.DatabaseInMemory ? sqlite : postgres;

    return DbFactory.getConnection(connectionConfig);
  }

  private static async getConnection(connectionConfig: ConnectionOptions): Promise<Connection> {
    const connectionManager = getConnectionManager();

    const connection: Connection = connectionManager.has("default")
      ? connectionManager.get()
      : connectionManager.create(connectionConfig);

    if (!connection.isConnected) {
      await connection.connect();
    }

    return connection;
  }
}

export { DbFactory };
