import { DbFactory } from "../DbFactory";

const up = async (): Promise<void> => {
  const connection = await DbFactory.Create();

  await connection.runMigrations({ transaction: "all" });
};

const show = async (): Promise<void> => {
  const connection = await DbFactory.Create();

  await connection.showMigrations();
};

const undo = async (): Promise<void> => {
  const connection = await DbFactory.Create();

  await connection.undoLastMigration({ transaction: "all" });
};

const execute = async (): Promise<void> => {
  const commands: string[] = ["up", "show", "undo"];

  const runnerArg = process.argv.find((arg: string): boolean => commands.includes(arg));

  if (runnerArg === undefined) {
    throw Error(`Invalid arg: ${runnerArg}`);
  }

  switch (runnerArg) {
    case "up":
      await up();
      break;
    case "show":
      await show();
      break;
    default:
      await undo();
  }
};

const run = async (): Promise<void> => {
  try {
    await execute();
    process.exit(0);
  } catch (ex) {
    // tslint:disable-next-line: no-console
    console.error(ex);

    process.exit(-1);
  }
};

// tslint:disable-next-line: no-floating-promises
run();
