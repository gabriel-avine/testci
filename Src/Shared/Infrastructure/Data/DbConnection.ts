import { injectable } from "inversify";
import { IDbConnection } from "Shared/Application/Contracts/Infrastructure/IDbConnection";
import { Connection, EntitySchema, QueryRunner, Repository } from "typeorm";

@injectable()
class DbConnection implements IDbConnection {
  public readonly queryRunner: QueryRunner;

  public constructor(connection: Connection) {
    this.queryRunner = connection.createQueryRunner();
  }

  public async beginTran(): Promise<void> {
    await this.queryRunner.startTransaction();
  }

  public async commit(): Promise<void> {
    await this.queryRunner.commitTransaction();
  }

  public getRepository<T>(entitySchema: EntitySchema): Repository<T> {
    return this.queryRunner.manager.getRepository<T>(entitySchema);
  }

  public async rollback(): Promise<void> {
    await this.queryRunner.rollbackTransaction();
  }
}

export { DbConnection };
