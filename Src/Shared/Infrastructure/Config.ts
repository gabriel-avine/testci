import { IConfig } from "Shared/Application/Contracts/IConfig";
import { LoggerOptions } from "typeorm/logger/LoggerOptions";

const DECIMAL = 10;

class Config implements IConfig {
  public get AppName(): string {
    return this.getValueOrDefault("APP_NAME");
  }

  public get DatabaseHost(): string {
    return this.getValueOrDefault("DATABASE_HOST");
  }

  public get DatabaseInMemory(): boolean {
    return this.getBooleanValue("DATABASE_IN_MEMORY");
  }

  public get DatabaseLogging(): LoggerOptions {
    const values = this.getValueOrDefault("DATABASE_LOGGING");

    if (values === "") {
      return false;
    }

    return values.split(",") as LoggerOptions;
  }

  public get DatabaseName(): string {
    return this.getValueOrDefault("DATABASE_NAME");
  }

  public get DatabasePassword(): string {
    return this.getValueOrDefault("DATABASE_PASSWORD");
  }

  public get DatabasePort(): number {
    const port = this.getValueOrDefault("DATABASE_PORT");

    return parseInt(port, DECIMAL);
  }

  public get DatabaseUser(): string {
    return this.getValueOrDefault("DATABASE_USER");
  }

  public get EnableSwagger(): boolean {
    return this.getBooleanValue("ENABLE_SWAGGER");
  }

  public get LogLevel(): string {
    return this.getValueOrDefault("LOG_LEVEL");
  }

  public get ShowError(): boolean {
    return this.getBooleanValue("SHOW_ERROR");
  }

  public get SnsApiVersion(): string {
    return this.getValueOrDefault("SNS_API_VERSION");
  }

  public get SnsAwsRegion(): string {
    return this.getValueOrDefault("SNS_AWS_REGION");
  }

  public get SnsTopicArn(): string {
    return this.getValueOrDefault("SNS_TOPIC_ARN");
  }

  private getBooleanValue(name: string): boolean {
    const value = this.getValueOrDefault(name);

    if (value === "") {
      return false;
    }

    return Boolean(JSON.parse(value));
  }

  private getValueOrDefault(name: string, defaultValue: string = ""): string {
    const value = process.env[name];

    if (value === undefined) {
      return defaultValue;
    }

    return value.toString();
  }
}

export { Config };
