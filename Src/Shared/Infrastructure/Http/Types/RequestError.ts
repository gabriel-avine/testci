/* tslint:disable: interface-name max-classes-per-file */
interface RequestError {
  config: RequestData;
  message: string;
  name: string;
  stack: string;
}

interface RequestData {
  baseURL: string;
  data: string;
  headers: object;
  method: string;
  url: string;
}

export { RequestError, RequestData };
