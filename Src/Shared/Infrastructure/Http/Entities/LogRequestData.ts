import { RequestData } from "../Types/RequestError";

class LogRequestData {
  public data: string;
  public headers: object;
  public method: string;
  public url: string;
  private readonly headerBlacklist: string[] = ["authorization", "x-api-key"];

  public constructor(config: RequestData) {
    this.url = `${config.baseURL}/${config.url}`;
    this.data = config.data;
    this.method = config.method;
    this.headers = this.getHeaders(config.headers);
  }

  public toJson(): string {
    return JSON.stringify(this);
  }

  private getHeaders(requestHeaders: object): object {
    const keys = Object.keys(requestHeaders);

    const headers: object = {};

    keys
      .filter((key: string): boolean => !this.headerBlacklist.includes(key.toLowerCase()))
      .forEach((item: string): boolean => (headers[item] = requestHeaders[item]));

    return headers;
  }
}
export { LogRequestData };
