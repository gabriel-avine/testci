import { AxiosError } from "axios";
import { RequestError } from "../Types/RequestError";
import { LogRequestData } from "./LogRequestData";
import { LogResponseData } from "./LogResponseData";

class LogRequestError {
  public request: LogRequestData;
  public response: LogResponseData;
  public stack: string;

  public constructor(axiosError: AxiosError) {
    const value = axiosError.toJSON();
    const error = value as RequestError;
    this.response = new LogResponseData(axiosError.response?.data, error);
    this.stack = error.stack;

    this.request = new LogRequestData(error.config);
  }
}

export { LogRequestError };
