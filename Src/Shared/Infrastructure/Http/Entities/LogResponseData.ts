import { RequestError } from "../Types/RequestError";

class LogResponseData {
  public data: object;
  public message?: string;
  public name?: string;

  public constructor(data: object, error?: RequestError) {
    this.message = error?.message;
    this.name = error?.name;
    this.data = data;
  }

  public toJson(): string {
    return JSON.stringify(this);
  }
}
export { LogResponseData };
