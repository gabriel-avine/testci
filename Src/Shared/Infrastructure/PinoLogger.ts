import { default as pino } from "pino";
import { IConfig } from "Shared/Application/Contracts/IConfig";
import { ILogger } from "Shared/Application/Contracts/Infrastructure/ILogger";
import { parseError } from "Shared/Utils/ParseError";

class PinoLogger implements ILogger {
  private readonly _correlationId: string;
  private readonly logger: pino.Logger;

  public constructor(correlationId: string, config: IConfig) {
    this._correlationId = correlationId;

    this.logger = pino({
      changeLevelName: "log-level",
      level: config.LogLevel,
      messageKey: "message",
      name: config.AppName,
      timestamp: (): string => `,"date": "${new Date().toISOString()}"`,
      useLevelLabels: true,
    });
  }

  public get correlationId(): string {
    return this._correlationId;
  }

  public debug(message: string, params: object): void {
    this.logger.debug({ ...params, correlationId: this._correlationId }, message);
  }

  public error(message: string, ex?: Error, params?: object): void {
    const log = {
      correlationId: this._correlationId,
      ...params,
      error: parseError(ex),
    };

    this.logger.error({ ...log }, message);
  }

  public info(message: string, params: object): void {
    this.logger.info({ ...params, correlationId: this._correlationId }, message);
  }

  public warn(message: string, params: object): void {
    this.logger.warn({ ...params, correlationId: this._correlationId }, message);
  }
}

export { PinoLogger };
