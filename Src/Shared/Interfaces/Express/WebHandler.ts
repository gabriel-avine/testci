import { plainToClass } from "class-transformer";
import { ClassType } from "class-transformer/ClassTransformer";
import { Request, Response } from "express";
import { ILogger } from "Shared/Application/Contracts/Infrastructure/ILogger";
import { IUseCase } from "Shared/Application/Contracts/UseCases/IUseCase";
import { BaseRequest } from "Shared/Application/Entities/BaseRequest";
import { Config } from "Shared/Infrastructure/Config";
import { Logger } from "Shared/Interfaces/DI/Types";
import { v4 as uuid } from "uuid";
import { ServiceProvider } from "../DI/ServiceProvider";
import { StatusCode } from "../Enums/StatusCode";
import { ExpressResult } from "./Entities/ExpressResult";

class WebHandler {
  private readonly headerBlackllist: string[] = ["x-api-key", "api-key"];
  private readonly methodsHasBody: string[] = ["POST", "PUT", "PATCH"];
  private readonly req: Request;
  private readonly requestType?: ClassType<BaseRequest>;
  private readonly res: Response;
  private readonly useCaseType: symbol;

  public constructor(req: Request, res: Response, useCaseType: symbol, requestType?: ClassType<BaseRequest>) {
    this.req = req;
    this.res = res;
    this.useCaseType = useCaseType;
    this.requestType = requestType;
  }

  public async handle(): Promise<void> {
    try {
      const correlationId = this.correlationId();

      const container = await ServiceProvider.GetContainer(correlationId);

      const logger: ILogger = container.get(Logger);

      this.requestLog(logger);

      const request = this.buildBaseRequest();

      const useCase: IUseCase = container.get(this.useCaseType);

      const result = await this.execute(useCase, request);

      logger.debug("Response", { response: result });

      this.res.setHeader("x-correlation-id", correlationId);

      this.res.status(result.status).json(result.body);
    } catch (ex) {
      const config = new Config();

      if (config.ShowError) {
        this.res.status(StatusCode.ERROR).json({ ex });
      } else {
        this.res.status(StatusCode.ERROR);
      }
    }
  }

  private buildBaseRequest(): BaseRequest | undefined {
    if (this.requestType === undefined) {
      return undefined;
    }

    let params = {};

    if (this.methodsHasBody.includes(this.req.method)) {
      params = this.req.body;
    }

    const paramsKeys = Object.keys(this.req.params);

    paramsKeys.forEach((key: string): void => {
      params[key] = this.req.params[key];
    });

    const queryString = Object.keys(this.req.query);

    queryString.forEach((key: string): void => {
      params[key] = this.req.query[key];
    });

    return plainToClass(this.requestType, params);
  }

  private correlationId(): string {
    const correlationId = this.req.headers["x-correlation-id"];

    if (correlationId !== undefined) {
      return correlationId.toString();
    }

    return uuid();
  }

  private async execute(useCase: IUseCase, request?: BaseRequest): Promise<ExpressResult> {
    if (request !== undefined && !request.isValid) {
      return ExpressResult.Invalid(request?.notifications);
    }

    const applicationResult = await useCase.execute(request);

    return new ExpressResult(applicationResult);
  }

  private getHeahders(): object {
    const keys = Object.keys(this.req.headers);

    const headers: object = {};

    keys
      .filter((key: string): boolean => !this.headerBlackllist.includes(key.toLowerCase()))
      .forEach((item: string): string | string[] | undefined => (headers[item] = this.req.headers[item]));

    return headers;
  }

  private requestLog(logger: ILogger): void {
    const request = {
      body: this.req.body,
      header: this.getHeahders(),
      method: this.req.method,
      url: this.req.baseUrl,
    };

    logger.debug("BaseRequest", { request });
  }
}

export { WebHandler };
