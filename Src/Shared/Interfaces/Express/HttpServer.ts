import bodyParser from "body-parser";
import { default as cors } from "cors";
import { default as express } from "express";
import helmet from "helmet";
import { Config } from "Shared/Infrastructure/Config";
import * as document from "Shared/lapag-ledger-service.json";
import { default as swaggerUi } from "swagger-ui-express";
import * as Types from "../DI/Types";
import { Adapter } from "./Adapter";

class HttpServer {
  public static Create(): express.Application {
    const server: express.Application = express();

    // tslint:disable-next-line: deprecation
    server.use(bodyParser.text());
    // tslint:disable-next-line: deprecation
    server.use(bodyParser.json());
    server.use(cors());
    server.use(helmet());

    const config = new Config();

    server.get("/health", Adapter(Types.HealthCheckUseCase));

    if (config.EnableSwagger) {
      server.use("/swagger/", swaggerUi.serve);
      server.get("/swagger", swaggerUi.setup(document));
    }

    return server;
  }
}

export { HttpServer };
