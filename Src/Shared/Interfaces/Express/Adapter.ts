import { ClassType } from "class-transformer/ClassTransformer";
import { Request as ExpressRequest, Response } from "express";
import { BaseRequest } from "Shared/Application/Entities/BaseRequest";
import { WebHandler } from "./WebHandler";

// tslint:disable-next-line: typedef
const Adapter = (useCaseType: symbol, requestType?: ClassType<BaseRequest>) => async (
  req: ExpressRequest,
  res: Response
): Promise<void> => {
  const webHandler = new WebHandler(req, res, useCaseType, requestType);
  await webHandler.handle();
};

export { Adapter };
