import { ApplicationResult } from "Shared/Application/Entities/ApplicationResult";
import { Notification } from "Shared/Application/Entities/Notification";
import { ApplicationEvents } from "Shared/Application/Enums/ApplicationEvents";
import { StatusCode } from "../../Enums/StatusCode";

class ExpressResult {
  public static Error(ex?: Error): ExpressResult {
    const applicationResult = new ApplicationResult(ApplicationEvents.ERROR, ex);

    return new ExpressResult(applicationResult);
  }

  public static Invalid(notifications: Notification[]): ExpressResult {
    const applicationResult = new ApplicationResult(ApplicationEvents.INVALID_EXECUTION, notifications);

    return new ExpressResult(applicationResult);
  }

  public body?: object;
  public status: StatusCode;

  public constructor(applicationResult: ApplicationResult) {
    this.status = this.getCode(applicationResult);
    this.body = applicationResult.message;
  }

  private getCode(result: ApplicationResult): StatusCode {
    if (result.event === ApplicationEvents.SUCCESS && result.message === undefined) {
      return StatusCode.NO_CONTENT;
    }

    switch (result.event) {
      case ApplicationEvents.ERROR:
        return StatusCode.ERROR;
      case ApplicationEvents.INVALID_EXECUTION:
        return StatusCode.BAD_REQUEST;
      case ApplicationEvents.SUCCESS:
        return StatusCode.OK;
      case ApplicationEvents.SUCCESS_CREATED:
        return StatusCode.CREATED;
      case ApplicationEvents.NOT_FOUND:
        return StatusCode.NOT_FOUND;
      default:
        throw new Error(`Invalid event ${result.event}`);
    }
  }
}

export { ExpressResult };
