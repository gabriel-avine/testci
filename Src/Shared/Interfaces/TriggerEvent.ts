import { SQSRecord } from "aws-lambda";

class TriggerEvent {
  public httpMethod?: string;
  public path?: string;
  public Records?: SQSRecord[];
}

export { TriggerEvent };
