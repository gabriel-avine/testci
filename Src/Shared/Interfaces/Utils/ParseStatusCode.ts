import { ApplicationResult } from "Shared/Application/Entities/ApplicationResult";
import { ApplicationEvents } from "Shared/Application/Enums/ApplicationEvents";
import { StatusCode } from "../Enums/StatusCode";

const getStatusCode = (result: ApplicationResult): StatusCode => {
  if (result.event === ApplicationEvents.SUCCESS && result.message === undefined) {
    return StatusCode.NO_CONTENT;
  }

  switch (result.event) {
    case ApplicationEvents.ERROR:
      return StatusCode.ERROR;
    case ApplicationEvents.INVALID_EXECUTION:
      return StatusCode.BAD_REQUEST;
    case ApplicationEvents.SUCCESS:
      return StatusCode.OK;
    case ApplicationEvents.SUCCESS_CREATED:
      return StatusCode.CREATED;
    case ApplicationEvents.NOT_FOUND:
      return StatusCode.NOT_FOUND;
    default:
      throw new Error(`Invalid event ${result.event}`);
  }
};

export { getStatusCode };
