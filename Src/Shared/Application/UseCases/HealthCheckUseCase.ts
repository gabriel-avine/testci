import { injectable } from "inversify";
import { IHealthCheckUseCase } from "../Contracts/UseCases/IHealthCheckUseCase";
import { ApplicationResult } from "../Entities/ApplicationResult";
import { ApplicationEvents } from "../Enums/ApplicationEvents";

@injectable()
class HealthCheckUseCase implements IHealthCheckUseCase {
  public async execute(): Promise<ApplicationResult> {
    return new ApplicationResult(ApplicationEvents.SUCCESS);
  }
}
export { HealthCheckUseCase };
