export enum ApplicationEvents {
  ERROR = "error",
  INVALID_EXECUTION = "invalidExecution",
  SUCCESS = "success",
  SUCCESS_CREATED = "successCreated",
  NOT_FOUND = "notFound",
}
