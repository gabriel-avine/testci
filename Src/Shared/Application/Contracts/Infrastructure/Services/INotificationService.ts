import { DomainEvent } from "Shared/Domain/DomainEvent";

interface INotificationService {
  send(event: DomainEvent): Promise<boolean>;
}

export { INotificationService };
