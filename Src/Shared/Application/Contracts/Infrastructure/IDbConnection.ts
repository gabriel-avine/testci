import { EntitySchema, Repository } from "typeorm";

interface IDbConnection {
  beginTran(): Promise<void>;
  commit(): Promise<void>;
  getRepository<T>(entitySchema: EntitySchema): Repository<T>;
  rollback(): Promise<void>;
}

export { IDbConnection };
