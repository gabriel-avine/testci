class DomainEvent {
  public eventType: string;

  public constructor(eventType: string) {
    this.eventType = eventType;
  }
}
export { DomainEvent };
