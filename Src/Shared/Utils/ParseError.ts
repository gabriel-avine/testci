import { ErrorObject, serializeError } from "serialize-error";

const parseError = (ex?: Error): ErrorObject | undefined => {
  if (ex === undefined) {
    return undefined;
  }

  return serializeError(ex);
};

export { parseError };
