# LaPag Ledger

Prove of conception application to define module structures, used packages, test structure, ci configuration and other concepts

# Prerequisites:

Have installed:

[Docker](https://docs.docker.com/install/)

[Docker compose](https://docs.docker.com/compose/install/)

# Run

Install dependencies

```shell
yarn
```

To run database

```shell
docker-compose up db
```

To run application and database to develop:

```shell
docker-compose up
```

# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

- Quick summary
- Version
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up?

- Summary of set up
- Configuration
- Dependencies
- Database configuration
- How to run tests
- Deployment instructions

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Who do I talk to?

- Repo owner or admin
- Other community or team contact
  All changes on files is refreshed in docker-compose context.

Apllication run in http://localhost:3000 or EXPRESS_PORT environment variable and ts-node-dev inspect on http://localhost:9229

# Lint

To run application lint:

```shell
yarn lint
```

# Tests

Before tests use postgres database run migrations:

## Unit tests

To run unit tests:

To single execution:

```shell
yarn test:unit
```

To watch:

```shell
yarn test:unit:watch
```

The unit test environment variables is in `Scripts/test:unit.sh`

## Integration tests

Before running integration tests go up the database

```shell
docker-compose up db -d
```

To single execution:

```shell
yarn test:integration
```

To watch:

```shell
yarn test:integration:watch
```

The integration test environment variables is in `Scripts/test:integration.sh`.

Integration test context use postgres database.

## Coverage

To running local go up the database before.

```shell
docker-compose up db
```

and run

```shell
npm run test:coverage
```

# Migrations

To run local migrations:

Up:

```shell
npm run migrate:local:up
```

Undo last migration:

```shell
npm run migrate:local:undo
```

# Modules

The application has Modules:

- Shared: Used to shared code and is responsible to application boostrap.

## New modules

To create new modules create the module name folder in Src folder and create module configuration files:

- RUN_MODULENAME_MODULE: (The env variable responsible to enable module): Create the variable in env filels and create a property in Config.ts.

- ModuleNameRoutes.ts (The file responsible to register module http routes): Imports this file with feature flag in Shared/Interfaces/Express/HttpServer.ts.

- ModuleNameEntityMappings (File responsible to register module entities in orm): Imports this file with feature flag in Shared/Infrastructure/Data/EntityMappings.ts.

- ModuleNameMigrationsList (File responsible to register module migrations path):
  Imports this file with feature flag in Shared/Infrastructure/Data/Migrations/MigrationsList.ts

- ModuleNameServiceProvider (File responsible to register dependency injection for module): Importhis this file with feature flag in Shared/Interfaces/DI/ServiceProvider.ts

# Docs

## Swagger

The swagger definition is in `Docs` folder. Create the swagger definitions in `Docs`.

To generate swagger definition in single file run:

```bash
yarn generate:docs
```

This command will generate `lapag-ledger-service.yml`.
