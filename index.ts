import "reflect-metadata";

import { Context } from "aws-lambda";
import { createServer, proxy } from "aws-serverless-express";
import { HttpServer } from "Shared/Interfaces/Express/HttpServer";
import { TriggerEvent } from "Shared/Interfaces/TriggerEvent";

if (process.env.START_EXPRESS === "1") {
  const app = HttpServer.Create();

  // tslint:disable-next-line: strict-boolean-expressions
  const port = process.env.EXPRESS_PORT || "3000";

  app.listen(port, (): void => {
    // tslint:disable-next-line: no-console
    console.log(`Starting server at http://0.0.0.0:${port}`);
  });
}

// tslint:disable-next-line: no-any
export const handler: (event: TriggerEvent, context: Context) => object = (event: any, context: Context): object => {
  const app = HttpServer.Create();
  const server = createServer(app);

  return proxy(server, event, context);
};
